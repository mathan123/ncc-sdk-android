package com.agero.nccsdk.adt;

import java.io.Serializable;

/**
 * Created by james hermida on 8/22/17.
 */

public enum AdtTriggerType implements Serializable {

    START_MONITORING("START_MONITORING"),

    START_WIFI("START_DRIVING_BY_WIFI"),
    START_MOTION("START_DRIVING_BY_MOTION"),
    START_GEOFENCE("START_DRIVING_BY_GEOFENCE"),
    START_SERVER("START_DRIVING_BY_SERVER"),

    STOP_MOTION("STOP_DRIVING_BY_MOTION"),
    STOP_LOCATION_TIMER("STOP_DRIVING_BY_LOCATION_TIMER"),
    STOP_LOCATION_MODE("STOP_DRIVING_BY_LOCATION_MODE"),
    STOP_SERVER("STOP_DRIVING_BY_SERVER");

    private final String name;
    
    AdtTriggerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
