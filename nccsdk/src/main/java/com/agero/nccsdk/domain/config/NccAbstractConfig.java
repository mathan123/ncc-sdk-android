package com.agero.nccsdk.domain.config;

/**
 * Created by james hermida on 8/30/17.
 */

abstract class NccAbstractConfig implements NccConfig {

    private int maxBufferSize = 3;

    public int getMaxBufferSize() {
        return maxBufferSize;
    }

    public void setMaxBufferSize(int maxBufferSize) {
        this.maxBufferSize = maxBufferSize;
    }
}
