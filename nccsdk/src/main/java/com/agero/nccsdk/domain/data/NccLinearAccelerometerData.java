package com.agero.nccsdk.domain.data;

import java.util.Locale;

/**
 * Created by james hermida on 8/29/17.
 */

public class NccLinearAccelerometerData extends NccAbstractSensorData {

    private final float x;
    private final float y;
    private final float z;

    /**
     * Creates an instance of NccLinearAccelerometerData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param x The acceleration the phone experiences minus the effects of gravity in the x direction
     * @param y The acceleration the phone experiences minus the effects of gravity in the y direction
     * @param z The acceleration the phone experiences minus the effects of gravity in the z direction
     */
    public NccLinearAccelerometerData(long timestamp, float x, float y, float z) {
        super(timestamp);
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Gets the acceleration the phone experiences minus the effects of gravity in the x direction
     *
     * @return float of the acceleration the phone experiences minus the effects of gravity in the x direction
     */
    public float getX() {
        return x;
    }

    /**
     * Gets the acceleration the phone experiences minus the effects of gravity in the y direction
     *
     * @return float of the acceleration the phone experiences minus the effects of gravity in the y direction
     */
    public float getY() {
        return y;
    }

    /**
     * Gets the acceleration the phone experiences minus the effects of gravity in the z direction
     *
     * @return float of the acceleration the phone experiences minus the effects of gravity in the z direction
     */
    public float getZ() {
        return z;
    }

    @Override
    public String toString() {
        return "NccLinearAccelerometerData{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", timestamp=" + timestamp +
                '}';
    }
}
