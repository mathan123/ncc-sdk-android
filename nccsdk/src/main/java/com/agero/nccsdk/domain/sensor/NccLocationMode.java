package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccLocationSettingsData;
import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 11/14/17.
 */

public class NccLocationMode extends NccAbstractBroadcastReceiverSensor {

    public NccLocationMode(Context context, NccConfig config) {
        super(context, NccLocationMode.class.getSimpleName(), NccSensorType.LOCATION_MODE, config);
    }

    @Override
    String[] getActions() {
        String action = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ? LocationManager.MODE_CHANGED_ACTION : LocationManager.PROVIDERS_CHANGED_ACTION;
        return new String[] { action };
    }

    @Override
    boolean isLocalReceiver() {
        return false;
    }

    @Override
    public void onDataReceived(Context context, Intent intent) {
        // Android 4.4 KitKat introduced Location modes
        NccLocationSettingsData locationModeData;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            NccLocationSettingsData.Mode mode = NccLocationSettingsData.Mode.UNAVAILABLE;
            try {
                int locationSetting = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
                switch (locationSetting) {
                    case Settings.Secure.LOCATION_MODE_OFF:
                        mode = NccLocationSettingsData.Mode.OFF;
                        break;
                    case Settings.Secure.LOCATION_MODE_SENSORS_ONLY:
                        mode = NccLocationSettingsData.Mode.SENSORS_ONLY;
                        break;
                    case Settings.Secure.LOCATION_MODE_BATTERY_SAVING:
                        mode = NccLocationSettingsData.Mode.BATTERY_SAVING;
                        break;
                    case Settings.Secure.LOCATION_MODE_HIGH_ACCURACY:
                        mode = NccLocationSettingsData.Mode.HIGH_ACCURACY;
                        break;
                }
            } catch (Settings.SettingNotFoundException snfe) {
                Timber.w(snfe, "Location mode setting not available on device");
            }

            locationModeData = new NccLocationSettingsData(System.currentTimeMillis(), mode);

            // Below Android 4.4 KitKit, GPS and network providers are available
        } else {
            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            NccLocationSettingsData.Provider provider =
                    new NccLocationSettingsData.Provider(
                            manager.isProviderEnabled(LocationManager.GPS_PROVIDER),
                            manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                    );
            locationModeData = new NccLocationSettingsData(System.currentTimeMillis(), provider);
        }

        postData(locationModeData);
    }
}
