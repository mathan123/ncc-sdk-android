package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccAbstractSensorData;
import com.agero.nccsdk.domain.data.NccRotationVectorData;

/**
 * Created by james hermida on 8/24/17.
 */

public class NccRotationVector extends NccAbstractNativeSensor {

    /**
     * Creates an instance of NccRotationVector
     *
     * @param context The Context allowing access to application-specific resources and classes, as well as
     * up-calls for application-level operations such as launching activities,
     * broadcasting and receiving intents, etc.
     * @throws NccException Exception if there is an error constructing the instance
     */
    public NccRotationVector(Context context, NccConfig config) throws NccException {
        super(context, NccRotationVector.class.getSimpleName(), NccSensorType.ROTATION_VECTOR, config);
    }

    /**
     * See {@link NccAbstractNativeSensor#buildData(SensorEvent, long)}
     */
    @Override
    protected NccAbstractSensorData buildData(SensorEvent sensorEvent, long sensorEventTime) {
        float[] quaternionValues = new float[4];
        SensorManager.getQuaternionFromVector(quaternionValues, sensorEvent.values);

        return new NccRotationVectorData(sensorEventTime,
            quaternionValues[0],
            quaternionValues[1],
            quaternionValues[2],
            quaternionValues[3]);
    }
}
