package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.content.Intent;

public class NccRunnable implements Runnable {

    private final Context context;
    private final Intent intent;
    private final NccBroadcastReceiverDelegate nccBroadcastReceiverDelegate;

    NccRunnable(Context context, Intent intent, NccBroadcastReceiverDelegate nccBroadcastReceiverDelegate) {
        this.context = context;
        this.intent = intent;
        this.nccBroadcastReceiverDelegate = nccBroadcastReceiverDelegate;
    }

    @Override
    public void run() {
        nccBroadcastReceiverDelegate.onDataReceived(context, intent);
    }
}
