package com.agero.nccsdk.internal.di.qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by james hermida on 10/18/17.
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface SdkLocalFilesPath {
}
