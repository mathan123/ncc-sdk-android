package com.agero.nccsdk.internal.log;

import com.agero.nccsdk.BuildConfig;
import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccExceptionErrorCode;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.data.network.aws.kinesis.KinesisClient;
import com.agero.nccsdk.internal.data.network.aws.model.LogPayload;
import com.agero.nccsdk.internal.log.config.LogConfig;
import com.agero.nccsdk.internal.common.util.Clock;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import static com.agero.nccsdk.internal.log.Util.getLogLevel;
import static com.agero.nccsdk.internal.common.util.ExceptionUtils.getStacktraceString;

/**
 * Created by james hermida on 11/13/17.
 */

public class DebugTree extends Timber.DebugTree {

    private final LogConfig config;
    private final DataManager dataManager;
    private final KinesisClient kinesisClient;
    private final Clock clock;

    private final Gson gson;

    public DebugTree(LogConfig logConfig, DataManager dataManager, KinesisClient kinesisClient, Clock clock) {
        this.config = logConfig;
        this.dataManager = dataManager;
        this.kinesisClient = kinesisClient;
        this.clock = clock;
        this.gson = new Gson();
    }

    @Override
    protected void log(int priority, String tag, @NotNull String message, Throwable t) {
        logToConsole(priority, tag, message, t);

        // Report logs to Kinesis Firehose based on config
        if (getLogLevel(priority) >= config.getMinLogLevel()) {
            long timeUtc = clock.currentTimeMillis();
            String timeLocal = clock.getLocalTimestamp(timeUtc);

            LogPayload logPayload = new LogPayload(getLogLevel(priority), dataManager.getUserId(), message);
            logPayload.setTimeUtc(timeUtc);
            logPayload.setTimeLocal(timeLocal);
            if (dataManager.getTrackingContext() != null) {
                logPayload.setMetadata(dataManager.getTrackingContext().getData());
            }

            if (getLogLevel(priority) == LogLevel.ERROR.getId() && t != null) {
                logPayload.setStacktrace(getStacktraceString(t));
            }

            kinesisClient.save(gson.toJson(logPayload), BuildConfig.KINESIS_STREAM_LOGS);
        }
    }

    @Override
    protected String createStackElementTag(@NotNull StackTraceElement element) {
        return super.createStackElementTag(element) + ":" + element.getLineNumber();
    }

    private void logToConsole(int priority, String tag, @NotNull String message, Throwable t) {
        // Append stacktrace to message only when logging to console
        if (t != null) {
            String stacktrace = "\n" + getStacktraceString(t);
            if (t instanceof NccException) {
                NccException ne = (NccException) t;
                // Special case for NccExceptions
                // Stack traces will be logged to console for all error codes except SENSOR_UNAVAILABLE
                if (ne.getErrorCode() == NccExceptionErrorCode.SENSOR_UNAVAILABLE) {
                    message += ": " + ne.getMessage();
                } else {
                    message += stacktrace;
                }
            } else {
                message += stacktrace;
            }
        }

        super.log(priority, tag, message, t);
    }

    // TODO handle config changes
}
