package com.agero.nccsdk.lbt;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.SensorManager;
import com.agero.nccsdk.domain.config.NccLocationConfig;
import com.agero.nccsdk.domain.config.NccMotionConfig;
import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.lbt.network.LbtSyncManager;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by james hermida on 11/6/17.
 */

public class LbtCollectionState extends LbtAbstractState {

    public static final String ACTION_LBT = "ACTION_LBT";

    private final NccSensorType[] defaultLbtSensors = {
            NccSensorType.LOCATION,
            NccSensorType.MOTION_ACTIVITY
    };

    private LbtBufferedSensorListener<NccLocationData> locationListener;
    private LbtBufferedSensorListener<NccMotionData> motionListener;

    @Inject
    SensorManager sensorManager;

    @Inject
    DataManager dataManager;

    @Inject
    LbtSyncManager lbtSyncManager;

    @Inject
    Context context;

    LbtCollectionState(List<NccSensorListener> listeners) {
        this.trackingListeners = listeners;
    }

    @Override
    public void onEnter() {
        Timber.d("Entered LbtCollectionState");
        NccSdk.getComponent().inject(this);
        subscribeClientListeners(trackingListeners);
        subscribeInternalListeners();
        sendActionBroadcast(LbtAction.START);
    }

    @Override
    public void onExit() {
        Timber.d("Exited LbtCollectionState");
        unsubscribeClientListeners(trackingListeners);
        unsubscribeInternalListeners();
        sendActionBroadcast(LbtAction.STOP);
    }

    @Override
    public boolean addTrackingListener(StateMachine<LbtState> machine, NccSensorListener sensorListener) {
        if (super.addTrackingListener(machine, sensorListener)) {
            subscribeClientListener(sensorListener);
            return true;
        }

        return false;
    }

    @Override
    public boolean removeTrackingListener(StateMachine<LbtState> machine, NccSensorListener sensorListener) {
        if (super.removeTrackingListener(machine, sensorListener)) {
            unsubscribeClientListener(sensorListener);
            return true;
        }

        return false;
    }

    @Override
    public void onDrivingStart(StateMachine<LbtState> machine) {
        // DO NOTHING
    }

    @Override
    public void onDrivingStop(StateMachine<LbtState> machine) {
        machine.setState(new LbtInactiveState(trackingListeners));
    }

    private void subscribeClientListeners(List<NccSensorListener> listeners) {
        for (NccSensorListener listener : listeners) {
            subscribeClientListener(listener);
        }
    }

    private void subscribeClientListener(NccSensorListener sensorListener) {
        for (NccSensorType sensorType : defaultLbtSensors) {
            try {
                sensorManager.subscribeSensorListener(sensorType, sensorListener, dataManager.getConfig(sensorType));
            } catch (NccException ne) {
                Timber.e(ne, "Exception subscribing client listener %s to sensor %s", sensorListener.getClass().getName(), sensorType.getName());
            }
        }
    }

    private void subscribeInternalListeners() {
        // Subscribe internal SDK listener. These listeners stream data to the backend.
        try {
            NccLocationConfig config = (NccLocationConfig) dataManager.getConfig(NccSensorType.LOCATION);
            locationListener = new LbtBufferedSensorListener<>(lbtSyncManager, config.getMaxBufferSize());
            sensorManager.subscribeSensorListener(NccSensorType.LOCATION, locationListener, config);
        } catch (NccException ne) {
            Timber.e(ne, "Exception subscribing internal listener to sensor %s", NccSensorType.LOCATION.getName());
        }

        try {
            NccMotionConfig config = (NccMotionConfig) dataManager.getConfig(NccSensorType.MOTION_ACTIVITY);
            motionListener = new LbtBufferedSensorListener<>(lbtSyncManager, config.getMaxBufferSize());
            sensorManager.subscribeSensorListener(NccSensorType.MOTION_ACTIVITY, motionListener, config);
        } catch (NccException ne) {
            Timber.e(ne, "Exception subscribing internal listener to sensor %s", NccSensorType.MOTION_ACTIVITY.getName());
        }
    }

    private void unsubscribeClientListeners(List<NccSensorListener> listeners) {
        for (NccSensorListener listener : listeners) {
            unsubscribeClientListener(listener);
        }
    }

    private void unsubscribeClientListener(NccSensorListener sensorListener) {
        for (NccSensorType sensorType : defaultLbtSensors) {
            try {
                sensorManager.unsubscribeSensorListener(sensorType, sensorListener);
            } catch (NccException ne) {
                Timber.e(ne, "Exception unsubscribing client listener %s to sensor %s", sensorListener.getClass().getName(), sensorType.getName());
            }
        }
    }

    private void unsubscribeInternalListeners() {
        try {
            sensorManager.unsubscribeSensorListener(NccSensorType.LOCATION, locationListener);
        } catch (NccException ne) {
            Timber.e(ne, "Exception unsubscribing internal listener to sensor %s", NccSensorType.LOCATION.getName());
        }

        try {
            sensorManager.unsubscribeSensorListener(NccSensorType.MOTION_ACTIVITY, motionListener);
        } catch (NccException ne) {
            Timber.e(ne, "Exception unsubscribing internal listener to sensor %s", NccSensorType.MOTION_ACTIVITY.getName());
        }
    }

    private void sendActionBroadcast(LbtAction lbtAction) {
        Intent i = new Intent(ACTION_LBT);
        i.putExtra(ACTION_LBT, lbtAction);
        LocalBroadcastManager.getInstance(context).sendBroadcast(i);
    }
}
