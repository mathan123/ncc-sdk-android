package com.agero.nccsdk.lbt;

import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.internal.common.statemachine.State;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;

/**
 * Created by james hermida on 11/6/17.
 */

public interface LbtState extends State {

    boolean addTrackingListener(StateMachine<LbtState> machine, NccSensorListener sensorListener);

    boolean removeTrackingListener(StateMachine<LbtState> machine, NccSensorListener sensorListener);

    void onDrivingStart(StateMachine<LbtState> machine);

    void onDrivingStop(StateMachine<LbtState> machine);
}
