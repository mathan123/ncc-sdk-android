package com.agero.nccsdk.ubi;

import com.agero.nccsdk.adt.event.AdtEventListener;
import com.agero.nccsdk.internal.common.statemachine.AbstractStateMachine;

/**
 * Created by james hermida on 8/22/17.
 */

public class UbiStateMachine extends AbstractStateMachine<UbiState> implements AdtEventListener {

    public UbiStateMachine(UbiState state) {
        super(state);
    }

    @Override
    public void onDrivingStart() {
        currentState.startCollecting(this);
    }

    @Override
    public void onDrivingStop() {
        currentState.stopCollecting(this);
    }

    public String getCurrentSessionId() {
        return currentState instanceof UbiCollectionState
                ? ((UbiCollectionState) currentState).getSessionId()
                : "";
    }
}
