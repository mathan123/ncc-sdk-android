package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccAccelerometerData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/26/17.
 */

public class NccAccelerometerMapper extends AbstractSensorDataMapper {

    @Override
    public String map(NccSensorData data) {
        NccAccelerometerData ad = (NccAccelerometerData) data;
        clearStringBuilder();
        sb.append("RA,");                               // Code
        sb.append(ad.getX()); sb.append(",");           // userAx
        sb.append(ad.getY()); sb.append(",");           // userAy
        sb.append(ad.getZ()); sb.append(",");           // userAz
        sb.append("NA,NA,NA,NA,NA,NA,");
        sb.append(ad.getTimestamp()); sb.append(",");   // UTCTime
        sb.append(getReadableTimestamp(ad.getTimestamp())); // Timestamp
        return sb.toString();
    }
}
