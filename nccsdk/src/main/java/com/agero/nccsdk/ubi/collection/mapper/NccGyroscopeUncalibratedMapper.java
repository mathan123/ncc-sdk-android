package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccGyroscopeUncalibratedData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccGyroscopeUncalibratedMapper extends AbstractSensorDataMapper {

    @Override
    public String map(NccSensorData data) {
        NccGyroscopeUncalibratedData gud = (NccGyroscopeUncalibratedData) data;
        clearStringBuilder();
        sb.append("RR,");                               // Code
        sb.append(gud.getRotationX()); sb.append(",");  // Rx
        sb.append(gud.getRotationY()); sb.append(",");  // Ry
        sb.append(gud.getRotationZ()); sb.append(",");  // Rz
        sb.append(gud.getDriftX()); sb.append(",");     // Dx
        sb.append(gud.getDriftY()); sb.append(",");     // Dy
        sb.append(gud.getDriftZ()); sb.append(",");     // Dz
        sb.append("NA,NA,NA,");
        sb.append(gud.getTimestamp()); sb.append(",");  // UTCTime
        sb.append(getReadableTimestamp(gud.getTimestamp())); // Timestamp
        return sb.toString();
    }
}
