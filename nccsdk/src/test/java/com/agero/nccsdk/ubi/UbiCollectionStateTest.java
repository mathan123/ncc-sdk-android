package com.agero.nccsdk.ubi;

import android.content.Context;

import com.agero.nccsdk.ubi.collection.SensorCollector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.verifyZeroInteractions;

/**
 * Created by james hermida on 11/16/17.
 */

@RunWith(PowerMockRunner.class)
public class UbiCollectionStateTest {
    
    private UbiCollectionState ubiCollectionState;

    @Mock
    private Context mockContext;

    @Mock
    private UbiStateMachine mockUbiStateMachine;

    @Mock
    private SensorCollector mockUbiSessionCollector;

    @Before
    public void setUp() throws Exception {
        ubiCollectionState = PowerMockito.spy(new UbiCollectionState(mockContext));
    }

    @Test
    public void startCollecting() {
        ubiCollectionState.startCollecting(mockUbiStateMachine);
        verifyZeroInteractions(mockUbiStateMachine);
    }

    @Test
    public void stopCollecting() {
        ubiCollectionState.stopCollecting(mockUbiStateMachine);
        verify(mockUbiStateMachine).setState(any(UbiInactiveState.class));
    }

    @Test
    @PrepareForTest({UbiCollectionState.class})
    public void onExit() throws Exception {
        mockPrivateField(ubiCollectionState, "ubiSessionCollector", mockUbiSessionCollector);

        ubiCollectionState.onExit();

        verify(mockUbiSessionCollector).stop();
    }

    private void mockPrivateField(Object targetObject, String fieldName, Object fieldValue) throws Exception {
        Field field = UbiCollectionState.class.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(targetObject, fieldValue);
    }
}
