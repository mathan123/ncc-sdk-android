package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccBarometerData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccBarometerMapperTest {

    private AbstractSensorDataMapper mapper = new NccBarometerMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        float pressureReading = 2.0185f;
        float conversion = NccBarometerMapper.HPA_TO_PASCAL_MULTIPLIER * pressureReading;
        NccSensorData data = new NccBarometerData(timeUtc, pressureReading);

        String expected = "P," + conversion + ",NA,NA,NA,NA,NA,NA,NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
