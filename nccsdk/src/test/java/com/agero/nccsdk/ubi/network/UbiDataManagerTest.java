package com.agero.nccsdk.ubi.network;

import android.content.Context;

import com.agero.nccsdk.internal.common.io.FileCompressor;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.common.util.Clock;
import com.agero.nccsdk.internal.common.util.DeviceUtils;
import com.agero.nccsdk.internal.common.util.FileUtils;
import com.agero.nccsdk.internal.data.network.aws.s3.FileTransferManager;
import com.agero.nccsdk.ubi.UbiState;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.UUID;
import java.util.concurrent.Executor;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(PowerMockRunner.class)
public class UbiDataManagerTest {

    private UbiDataManager ubiDataManager;

    @Mock
    private Context mockContext;

    @Mock
    private Executor mockExecutor;

    @Mock
    private FileTransferManager mockFileTransferManager;

    @Mock
    private File mockUbiDataPath;

    @Mock
    private Clock mockClock;

    @Mock
    private StateMachine<UbiState> mockUbiStateMachine;

    @Before
    public void setUp() throws Exception {
        Constructor[] ctors = UbiDataManager.class.getDeclaredConstructors();
        ubiDataManager = PowerMockito.spy((UbiDataManager) ctors[0].newInstance(mockContext, mockExecutor, mockClock, mockFileTransferManager, mockUbiDataPath, mockUbiStateMachine));
    }

    @Test
    @PrepareForTest({FileUtils.class, UbiDataManager.class})
    public void uploadSession_valid_file() throws Exception {
        File testFile = new File("");
        String testTargetDest = "target";
        PowerMockito.mockStatic(FileUtils.class);
        PowerMockito.when(FileUtils.class, "isNullOrDoesNotExist", testFile).thenReturn(false);
        PowerMockito.doReturn(testTargetDest).when(ubiDataManager, "getUploadS3Bucket");

        ubiDataManager.uploadSession(testFile);
        verify(mockFileTransferManager).uploadFile(testTargetDest, testFile);
    }

    @Test
    public void uploadSession_null_file() throws Exception {
        File testFile = null;

        try {
            ubiDataManager.uploadSession(testFile);
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
        }
    }

    @Test
    @PrepareForTest({UbiDataManager.class, FileUtils.class, File.class, UbiDataManager.class})
    public void uploadAllSessions_is_current_session() throws Exception {
        PowerMockito.mockStatic(FileUtils.class);
        PowerMockito.when(FileUtils.class, "isNullOrDoesNotExist", mockUbiDataPath).thenReturn(false);
        File testFile = mock(File.class);
        File[] testFiles = new File[] { testFile };
        PowerMockito.when(mockUbiDataPath, "listFiles").thenReturn(testFiles);

        mockIsCurrentSession(true);
        ubiDataManager.uploadAllSessions();
        verifyZeroInteractions(mockExecutor);
        verifyZeroInteractions(mockFileTransferManager);
    }

    @Test
    @PrepareForTest({UbiDataManager.class, FileUtils.class, File.class, UbiDataManager.class})
    public void uploadAllSessions_session_requires_compression() throws Exception {
        PowerMockito.mockStatic(FileUtils.class);
        PowerMockito.when(FileUtils.class, "isNullOrDoesNotExist", mockUbiDataPath).thenReturn(false);
        File testFile = mock(File.class);
        File[] testFiles = new File[] { testFile };
        PowerMockito.when(mockUbiDataPath, "listFiles").thenReturn(testFiles);
        PowerMockito.when(testFile, "isDirectory").thenReturn(true);
        PowerMockito.doReturn(true).when(ubiDataManager, "isValidUuid", anyString());
        ProcessSessionDataRunnable testProcessSessionDataRunnable = Mockito.mock(ProcessSessionDataRunnable.class);
        PowerMockito.doReturn(testProcessSessionDataRunnable).when(ubiDataManager, "getProcessSessionDataRunnable", testFile);

        mockIsCurrentSession(false);

        ubiDataManager.uploadAllSessions();
        verify(mockExecutor).execute(any(ProcessSessionDataRunnable.class));
    }

    @Test
    @PrepareForTest({UbiDataManager.class, FileUtils.class, DeviceUtils.class})
    public void uploadAllSessions_upload_compressed_session() throws Exception {
        PowerMockito.mockStatic(FileUtils.class);
        PowerMockito.when(FileUtils.class, "isNullOrDoesNotExist", mockUbiDataPath).thenReturn(false);
        File testFile = new File(UUID.randomUUID().toString() + FileCompressor.EXTENSION_TAR);
        File[] testFiles = new File[] { testFile };
        PowerMockito.when(mockUbiDataPath, "listFiles").thenReturn(testFiles);
        PowerMockito.mockStatic(DeviceUtils.class);
        PowerMockito.when(DeviceUtils.class, "isConnectedToWiFi", mockContext).thenReturn(true);

        mockIsCurrentSession(false);

        ubiDataManager.uploadAllSessions();
        verify(mockFileTransferManager).uploadFile(anyString(), any(File.class));
    }

    private void mockIsCurrentSession(boolean available) throws Exception {
        PowerMockito.doReturn(available).when(ubiDataManager, "isCurrentSession", anyString());
    }
}
